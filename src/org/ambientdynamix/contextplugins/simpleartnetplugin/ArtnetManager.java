/*
 * Author: Bashar Altakrouri
 * Project: AmbientWeb - AmbientLight Dynamix plugin
 * Affiliation: Institute of Telematics - University of Luebeck
 * Created: 26.09.2012
 * Last modified: 01.10.2012
 * 
 * Acknowledgment:
 * Some of the code used in this file is originally created by Karsten Schmidt (PostSpectacular Ltd.)
 * as part of of the artnet4j project.
 * 
 * artnet4j is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * artnet4j is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with artnet4j. If not, see <http://www.gnu.org/licenses/>.
 */

package org.ambientdynamix.contextplugins.simpleartnetplugin;


import java.net.SocketException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import android.net.NetworkInfo;
import android.util.Log;
import artnet4j.ArtNet;
import artnet4j.ArtNetException;
import artnet4j.ArtNetNode;
import artnet4j.events.ArtNetDiscoveryListener;
import artnet4j.packets.ArtDmxPacket;

public class ArtnetManager implements ArtNetDiscoveryListener {

	private static final String TAG = "AmbientWeb|ArtnetManager";
	private ArtNetNode netLynx;
	private int sequenceID;
	static ArtNet artnet;
	private static boolean startedDiscoveryFlag = false; 
	private static ArtnetManagerListener listener;
	private static List<ArtNetNode> discoveredNodesList;


	public static void startListening(ArtnetManagerListener artnetListener) {
		listener = artnetListener;
		listener.onStartedListening();	// fire onStartedListening event back to the client
		Log.i(TAG, "start listening to Artnet Manager");
	}


	@Override
	public void discoveredNewNode(ArtNetNode node) {
		if (netLynx == null) {
			netLynx = node;
			Log.i(TAG, "Artnet node discovered: " + node);
			listener.onDiscoveredNewNode(node);	// fire onDiscoveredNewNode event to subscribed clients


		}
	}

	@Override
	public void discoveredNodeDisconnected(ArtNetNode node) {
		Log.i(TAG, "Artnet node is disconnected: " + node);
		listener.onDiscoveredNodeDisconnected(node); 	// fire onDiscoveredNodeDisconnected event to subscribed clients
		if (node == netLynx) {
			netLynx = null;
		}
	}

	@Override
	public void discoveryCompleted(List<ArtNetNode> nodes) {


		Log.i(TAG,"Artnet discovery completed, discovered [" +  nodes.size()  + "] nodes");
		if (nodes.size() >0){

			listener.onDiscoveryCompleted(nodes); 	// fire onDiscoveryCompleted event to subscribed clients

			for (ArtNetNode n : nodes) {
				Log.i(TAG,"Node: " + n);

			}

			if (discoveredNodesList == null)
			{
				discoveredNodesList= nodes;	// assign the newly discovered list.
				listener.onDiscoveryCompleteUpadate(nodes); 	// fire onDiscoveryCompleted event to subscribed clients	
			}else{
				Set<ArtNetNode> intersect = new HashSet<ArtNetNode>(discoveredNodesList);
				intersect.retainAll(nodes);
				if (intersect.size() != discoveredNodesList.size()){
					discoveredNodesList= nodes;	// assign the newly discovered list.
					listener.onDiscoveryCompleteUpadate(nodes); 	// fire onDiscoveryCompleted event to subscribed clients	
				}
			}

		}else{
			Log.i(TAG,"No action to be taken, fine scan complete onDiscoveryCompleted event");
			listener.onDiscoveryCompleted(nodes); 	// fire onDiscoveryCompleted event to subscribed clients
		}
	}

	@Override
	public void discoveryFailed(Throwable t) {
		Log.e(TAG, "Discovery failed!");
		listener.onDiscoveryFailed(t); 	// fire onDiscoveryCompleted event to subscribed clients
	}

	/*
	 * Activate the discovery process to find the AmbientWeb Light node
	 */
	public void startDiscovery() {

		Log.i(TAG,"Start Artnet discovery");

		if (!startedDiscoveryFlag){
			artnet = new ArtNet();

			try {
				artnet.start();
				artnet.getNodeDiscovery().addListener(this);
				artnet.startNodeDiscovery();
				listener.onStartedDiscovery(); 	// fire onStartedDiscovery event to subscribed clients
				listener.onDiscoveryIsEnabled(); 	// fire onDiscoveryIsEnabled event to subscribed clients
				startedDiscoveryFlag = true;
				Log.i(TAG,"Artnet discovery started");

			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ArtNetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else
		{
			Log.i(TAG,"Artnet discovery is already started");
			listener.onDiscoveryIsEnabled(); 	// fire onDiscoveryIsEnabled event to subscribed clients, to indicate that the discovery process is active

		}
	}

	public boolean isDiscovering(){
		return startedDiscoveryFlag;
	}

	public List<ArtNetNode> getAvailableNodes(){
		return discoveredNodesList;
	}

	public void stopDiscovery() {
		startedDiscoveryFlag=false;
		listener.onStoppedDiscovery(); 	// fire onStoppedDiscovery event to subscribed clients
		artnet.getNodeDiscovery().removeListener(this);
		artnet.stop();
		Log.i(TAG, "Discovery stopped!");

	}

	/*
	 * turn node's light off
	 */
	public void switchLightOff(ArtNetNode netLynxNode){
		restRGB(netLynxNode);
	}

	/*
	 * Fade the color of a certain channel
	 * -1: White
	 * 0: Red
	 * 1: Green
	 * 2: Blue
	 * 
	 * period: fading period in milli
	 * repeat: wether to repeat the fading action or not
	 * keepOn: keep the lighting on after the fade.
	 * duration: duration of the light active after the fade (-1> infinity | 0: off immediately | >1: duration in milli)
	 */

	public void colorFade(ArtNetNode netLynxNode,  int channel, int period,  boolean keepOn, int duration){
		int sleepTime = period / 255;

		// with inner class threading	 
		Inner inner = new Inner(netLynxNode, channel, period, keepOn, duration);
	}

	private class Inner implements Runnable {
		Thread t;
		ArtNetNode netLynxNode;
		int channel;
		int period;
		int sleepTime;
		boolean keepOn;
		int duration;
		TurnLightOffTask turnOffTask;

		Inner(ArtNetNode n, int c, int p, boolean k, int d) {
			netLynxNode = n;
			channel = c;
			period = p;
			sleepTime = period / 255;
			keepOn=k;
			duration = d;
			t = new Thread(this, "Inner");
			t.start();
		}

		public void run() {
			// your code

			Log.i(TAG, "Fading on channel "  + channel);

			while (true){

				for (int i=0; i<254 ; ++i){
					if (channel<0)
						setNodeRGB(netLynxNode, i, i, i);
					else
						setChannelColor(netLynxNode, channel, i);
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				
				if (keepOn) // keepOn,  on infinity
				{
					if (channel<0)
						setNodeRGB(netLynxNode, 255, 255, 255);
					else
						setChannelColor(netLynxNode, channel, 255);

				}
				else	// swtich off
				{	
					if (duration < 0)	// on infinity
						if (channel<0)
							setNodeRGB(netLynxNode, 255, 255, 255);
						else
							setChannelColor(netLynxNode, channel, 255);

					else{
						Log.i(TAG, "Keep light on for "  + duration + "millisecond");
						Timer turnOffTaskTimer = new Timer("LightTurnOffTask");
						turnOffTask = new TurnLightOffTask(netLynxNode);
						turnOffTaskTimer.schedule(turnOffTask,duration, duration);
					}
				}
				break;
								
			}


		}
	}


	private class TurnLightOffTask extends TimerTask {
		ArtNetNode netLynxNode;

		TurnLightOffTask(ArtNetNode n) {
			netLynxNode = n;
		}
		public void run() {
			//Stop Timer.
			//Log.i(TAG, "Turn off light");

			restRGB(netLynxNode);
			this.cancel();
		}
	}


	/*
	 * Fade A CERTAIN RGB color
	 * 0: Red
	 * 1: Green
	 * 2: Blue
	 * 
	 * period: fading period in milli
	 * repeat: wether to repeat the fading action or not
	 * keepOn: keep the lighting on after the fade.
	 * duration: duration of the light active after the fade (-1> infinity | 0: off immediately | >1: duration in milli)
	 */

	public void fadeRGB(ArtNetNode netLynxNode,  int rChannel , int gChannel, int bChannel, int period,  boolean keepOn, int duration){
		int sleepTime = period / 255;

		// with inner class threading	 
		RGBFadeInner inner = new RGBFadeInner(netLynxNode, rChannel, gChannel, bChannel, period, keepOn, duration);
	}

	private class RGBFadeInner implements Runnable {
		Thread t;
		ArtNetNode netLynxNode;
		int rChannel;
		int gChannel;
		int bChannel;
		int period;
		int sleepTime;
		boolean keepOn;
		int duration;
		TurnLightOffTask turnOffTask;

		RGBFadeInner(ArtNetNode n, int r, int g, int b, int p, boolean k, int d) {
			netLynxNode = n;
			rChannel = r;
			gChannel = g;
			bChannel = b;

			period = p;
			sleepTime = period / 255;
			keepOn=k;
			duration = d;
			t = new Thread(this, "Inner");
			t.start();
		}

		public void run() {
			// your code

			Log.i(TAG, "Fading to RGB ["  + rChannel + "," + gChannel + "," + bChannel + "]");

			int rStep = rChannel / 255;
			int gStep = gChannel / 255;
			int bStep = bChannel / 255;


			while (true){

				for (int i=0; i<254 ; ++i){
					setNodeRGB(netLynxNode, Math.round(i * rStep), Math.round(i * gStep), Math.round(i * bStep));
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (keepOn) // keepOn,  on infinity
				{
						setNodeRGB(netLynxNode, rChannel, gChannel, bChannel);

				}
				else	// swtich off
				{	
					if (duration < 0)	// on infinity
						setNodeRGB(netLynxNode, rChannel, gChannel, bChannel);

					else{
						Log.i(TAG, "Keep light on for "  + duration + "millisecond");
						Timer turnOffTaskTimer = new Timer("LightTurnOffTask");
						turnOffTask = new TurnLightOffTask(netLynxNode);
						turnOffTaskTimer.schedule(turnOffTask,duration, duration);
					}
				}
				break;
			}

		}
	}


	/*
	 * Fade A CERTAIN RGB color
	 * 0: Red
	 * 1: Green
	 * 2: Blue
	 * 
	 * period: fading period in milli
	 * repeat: wether to repeat the fading action or not
	 * keepOn: keep the lighting on after the fade.
	 * duration: duration of the light active after the fade (-1> infinity | 0: off immediately | >1: duration in milli)
	 */

	public void fade2RGBColors(ArtNetNode netLynxNode,  int rChannelFrom , int gChannelFrom, int bChannelFrom, int rChannelTo , int gChannelTo, int bChannelTo, int period,  boolean keepOn, int duration){
		int sleepTime = period / 255;

		// with inner class threading	 
		RGB2FadeInner inner = new RGB2FadeInner(netLynxNode, rChannelFrom, gChannelFrom, bChannelFrom, rChannelTo, gChannelTo, bChannelTo, period, keepOn, duration);
	}

	private class RGB2FadeInner implements Runnable {
		Thread t;
		ArtNetNode netLynxNode;
		int rChannel;
		int gChannel;
		int bChannel;

		int rChannel2;
		int gChannel2;
		int bChannel2;

		int period;
		int sleepTime;
		boolean keepOn;
		int duration;
		TurnLightOffTask turnOffTask;

		RGB2FadeInner(ArtNetNode n, int r, int g, int b, int r2, int g2, int b2, int p, boolean k, int d) {
			netLynxNode = n;
			rChannel = r;
			gChannel = g;
			bChannel = b;

			rChannel2 = r2;
			gChannel2 = g2;
			bChannel2 = b2;

			period = p;
			sleepTime = period / 255;
			keepOn=k;
			duration = d;
			t = new Thread(this, "Inner");
			t.start();
		}

		public void run() {
			// your code

			Log.i(TAG, "Fading from RGB ["  + rChannel + "," + gChannel + "," + bChannel + "] to RGB ["  + rChannel2 + "," + gChannel2 + "," + bChannel2 + "]");

			int rStep = (rChannel2 - rChannel) / 255;
			int gStep = (gChannel2 - gChannel) / 255;
			int bStep = (bChannel2 - bChannel) / 255;


			while (true){

				for (int i=0; i<254 ; ++i){
					setNodeRGB(netLynxNode, rChannel + Math.round(i * rStep), gChannel + Math.round(i * gStep), bChannel + Math.round(i * bStep));
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (keepOn) // keepOn,  on infinity
				{
						setNodeRGB(netLynxNode, rChannel2, gChannel2, bChannel2);

				}
				else	// swtich off
				{	
					if (duration < 0)	// on infinity
						setNodeRGB(netLynxNode, rChannel2, gChannel2, bChannel2);

					else{
						Log.i(TAG, "Keep light on for "  + duration + "millisecond");
						Timer turnOffTaskTimer = new Timer("LightTurnOffTask");
						turnOffTask = new TurnLightOffTask(netLynxNode);
						turnOffTaskTimer.schedule(turnOffTask,duration, duration);
					}
				}
				break;
			}

		}
	}


	/*
	 * Set the node's channel color
	 * 0: Red
	 * 1: Green
	 * 2: Blue
	 */
	public boolean setChannelColor(ArtNetNode netLynxNode, int channel, int value){
		// if the node does not exist
		if (netLynxNode == null) {
			Log.e(TAG, "sending command to a non-existing node");
			return false ;
		}

		ArtDmxPacket dmx = new ArtDmxPacket();
		byte[] buffer = new byte[3];
		buffer[channel] = (byte) (value);
		dmx.setDMX(buffer, buffer.length);

		dmx.setUniverse(netLynxNode.getSubNet(),
				netLynxNode.getDmxOuts()[channel]);	// was 1
		artnet.unicastPacket(dmx, netLynxNode.getIPAddress());

		// Only for debugging
		/*
		switch (channel){
		case 0:
			Log.v("AmbientWeb|AmbientLight", "Node's Red light is set to"+ value +"  | [" +netLynxNode.getLongName()+ "]");
			break;
		case 1:
			Log.v("AmbientWeb|AmbientLight", "Node's Green light is set to"+ value +"  | [" +netLynxNode.getLongName()+ "]");
			break;
		case 2:
			Log.v("AmbientWeb|AmbientLight", "Node's Blue light is set to"+ value +"  | [" +netLynxNode.getLongName()+ "]");
			break;
		}
		 */
		return true; // success

	}


	/*
	 * Reset the node's RGB color (Switch the light off) 
	 */
	public boolean restRGB(ArtNetNode netLynxNode){
		// if the node does not exist
		if (netLynxNode == null) {
			Log.e(TAG, "sending command to a non-existing node");
			return false;
		}

		restChannel(netLynxNode, 0);
		restChannel(netLynxNode, 1);
		restChannel(netLynxNode, 2);
		//Log.i(TAG, "Node's light is rest / off | [" +netLynxNode.getLongName()+ "]");
		return true; // success
	}

	/*
	 * Reset the node's channel color (Switch the channel light off) 
	 * 
	 * 0: Red
	 * 1: Green
	 * 2: Blue
	 */
	public boolean restChannel(ArtNetNode netLynxNode, int channel){
		// if the node does not exist
		if (netLynxNode == null) {
			Log.e("AmbientWeb|AmbientLight", "sending command to a non-existing node");
			return false ;
		}

		ArtDmxPacket dmx = new ArtDmxPacket();
		byte[] buffer = new byte[3];
		buffer[channel] = (byte) (0);
		dmx.setDMX(buffer, buffer.length);
		
		dmx.setUniverse(netLynxNode.getSubNet(),
				netLynxNode.getDmxOuts()[0]);
		artnet.unicastPacket(dmx, netLynxNode.getIPAddress());
		
		dmx.setUniverse(netLynxNode.getSubNet(),
				netLynxNode.getDmxOuts()[1]);
		artnet.unicastPacket(dmx, netLynxNode.getIPAddress());

		// Only for debugging
		/*
		switch (channel){
		case 0:
			Log.v(TAG, "Node's Red light is rest / off | [" +netLynxNode.getLongName()+ "]");
			break;
		case 1:
			Log.v(TAG, "Node's Green light is rest / off | [" +netLynxNode.getLongName()+ "]");
			break;
		case 2:
			Log.v(TAG, "Node's Blue light is rest / off | [" +netLynxNode.getLongName()+ "]");
			break;
		}
		 */

		return true; // success

	}



	/*
	 * Send RGB color to the node
	 */
	public boolean setNodeRGB(ArtNetNode netLynxNode, int rChannel, int gChannel, int bChannel){

		// if the node does not exist
		if (netLynxNode == null) {
			Log.e(TAG, "sending command to a non-existing node");
			return false;
		}

		ArtDmxPacket dmx = new ArtDmxPacket();
		dmx.setUniverse(netLynxNode.getSubNet(),
				netLynxNode.getDmxOuts()[0]);
		dmx.setSequenceID(rChannel % 255);
		
		byte[] buffer = new byte[3];

		buffer[0] = (byte) (rChannel);	// R
		buffer[1] = (byte) (gChannel);	// G
		buffer[2] = (byte) (bChannel);	// B

		dmx.setDMX(buffer, buffer.length);
		artnet.unicastPacket(dmx, netLynxNode.getIPAddress());
		
		// Still to figure out why is this? what is the importance or the effect of this block
		dmx.setUniverse(netLynxNode.getSubNet(),
				netLynxNode.getDmxOuts()[1]);
		artnet.unicastPacket(dmx, netLynxNode.getIPAddress());
		
		return true; // success

	}

}


