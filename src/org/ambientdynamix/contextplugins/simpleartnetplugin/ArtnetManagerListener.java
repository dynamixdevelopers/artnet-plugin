package org.ambientdynamix.contextplugins.simpleartnetplugin;

import java.util.List;

import artnet4j.ArtNetNode;

public interface ArtnetManagerListener {

	public void onStartedListening();		// started listening
	public void onStartedDiscovery();		// started discovery
	public void onStoppedDiscovery();		// discovery is enabled already
	public void onDiscoveryIsEnabled();		// discovery is enabled already
	public void onDiscoveredNewNode(ArtNetNode node);
	public void onDiscoveredNodeDisconnected(ArtNetNode node);
	public void onDiscoveryCompleted(List<ArtNetNode> nodes);
	public void onDiscoveryCompleteUpadate(List<ArtNetNode> nodes);		// only when new discovery results
	public void onDiscoveryFailed(Throwable t);

}
