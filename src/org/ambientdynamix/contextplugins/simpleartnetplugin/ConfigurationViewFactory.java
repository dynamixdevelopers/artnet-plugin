/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.simpleartnetplugin;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;

import org.ambientdynamix.api.contextplugin.*;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Simple configuration interface for the plug-in. Shows how to programatically create the UI,  store settings and
 * set configured status. Also shows how to display an image that is contained in the plug-in's asset folder.
 * 
 * @author Darren Carlson
 * 
 */
public class ConfigurationViewFactory implements IContextPluginConfigurationViewFactory {
	private final String TAG = this.getClass().getSimpleName();

	@Override
	public void destroyView() {
		// Unused
	}

	@Override
	public View initializeView(Context context, final ContextPluginRuntime runtime, int titleBarHeight)
			throws Exception {
		// Discover our screen size for proper formatting
		DisplayMetrics met = context.getResources().getDisplayMetrics();
		// Access our Locale via the incoming context's resource configuration to determine language
		String language = context.getResources().getConfiguration().locale.getDisplayLanguage();
		LinearLayout lLayout = new LinearLayout(context);
		lLayout.setOrientation(LinearLayout.VERTICAL);
		lLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		TextView tView = new TextView(context);
		tView.setText("Click the button below to configure the plug-in with the current time.");
		tView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		lLayout.addView(tView);
		// Create the button
		final Button flashToggle = new Button(context);
		flashToggle.setText("Configure V2!");
		flashToggle.setMinimumWidth(150);
		flashToggle.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				ContextPluginSettings settings = new ContextPluginSettings();
				settings.put(SimpleArtnetPluginRuntime.SAMPLE_DATA, new Date().toString());
				if (runtime.getPluginFacade().storeContextPluginSettings(runtime.getSessionId(), settings)) {
					runtime.getPluginFacade().setPluginConfiguredStatus(runtime.getSessionId(), true);
					try {
						runtime.updateSettings(settings);
					} catch (Exception e) {
						Log.w(TAG, "Exception when updating settings: " + e);
					}
					runtime.getPluginFacade().closeConfigurationView(runtime.getSessionId());
				} else
					runtime.getPluginFacade().setPluginConfiguredStatus(runtime.getSessionId(), false);
			}
		});
		lLayout.addView(flashToggle);
		/*
		 * This section loads a sample image from the plug-ins JAR into an ImageView.
		 */
		ImageView image = new ImageView(context);
		try {
			// Grab the plug-in's ClassLoader using the SecuredContext
			ClassLoader classLoader = context.getClassLoader();
			if (classLoader != null) {
				// Create a URL to the resource you want
				URL url = classLoader.getResource("assets/agt_web.png");
				if (url != null) {
					// Load the image into a Bitmap using the BitmapFactory
					Bitmap bm = BitmapFactory.decodeStream(url.openStream());
					image.setImageBitmap(bm);
					// Add the Bitmap to the layout
					lLayout.addView(image);
				} else {
					Log.w(TAG, "URL was null");
				}
			}
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		}
		return lLayout;
	}
}