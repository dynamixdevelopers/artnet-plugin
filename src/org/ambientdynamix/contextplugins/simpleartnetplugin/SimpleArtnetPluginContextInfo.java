/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.simpleartnetplugin;

import java.util.HashSet;
import java.util.Set;

import org.ambientdynamix.api.application.IContextInfo;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

class SimpleArtnetPluginContextInfo implements IContextInfo, ISimpleArtnetPluginContextInfo {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<SimpleArtnetPluginContextInfo> CREATOR = new Parcelable.Creator<SimpleArtnetPluginContextInfo>() {
		public SimpleArtnetPluginContextInfo createFromParcel(Parcel in) {
			return new SimpleArtnetPluginContextInfo(in);
		}

		public SimpleArtnetPluginContextInfo[] newArray(int size) {
			return new SimpleArtnetPluginContextInfo[size];
		}
	};
	private final String TAG = this.getClass().getSimpleName();
	// Private data
	private String contextData;

	/* (non-Javadoc)
	 * @see org.ambientdynamix.contextplugins.sampleplugin.ISamplePluginContextInfo#getSampleData()
	 */
	@Override
	public String getSampleData() {
		return contextData;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.artnet";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return contextData;
		else
			return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	};

	public SimpleArtnetPluginContextInfo(String contextData) {
		this.contextData = contextData;
	}

	private SimpleArtnetPluginContextInfo(final Parcel in) {
		readFromParcel(in);
	}

	public IBinder asBinder() {
		return null;
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(contextData);
	}

	public void readFromParcel(Parcel in) {
		this.contextData = in.readString();
	}
}
