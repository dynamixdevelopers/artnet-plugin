/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.simpleartnetplugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.Message;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.contextplugins.ambientcontrol.CommandsEnum;
import org.ambientdynamix.contextplugins.ambientcontrol.ControlConnectionManager;
import org.ambientdynamix.contextplugins.ambientcontrol.IDisplayCommand;
import org.ambientdynamix.contextplugins.ambientcontrol.TranslatingProfileMatcher;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import artnet4j.ArtNetNode;

public class SimpleArtnetPluginRuntime extends ContextPluginRuntime implements ArtnetManagerListener {
	private final String TAG = this.getClass().getSimpleName();
	public static final String SAMPLE_DATA = "SAMPLE_DATA";
	private PrivacyRiskLevel PrivacyRisk;
	private String sampleData;
	private ControlConnectionManager controlMgr;
	// artnet
	private ArtnetManager myArtnetManager;
	private List<ArtNetNode> discoveredNodeList = new ArrayList<ArtNetNode>();
	private int red = 0;
	private int green = 0;
	private int blue = 0;
	private ControlConnectionManager.ControllConnectionManagerListenerAdapter serverListener = new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {
		@Override
		public void stopControlling(String arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void controllRequest(CommandsEnum arg0, String arg1) {
			// TODO Auto-generated method stub
		}

		@Override
		public void consumeDisplayCommand(IDisplayCommand command, String sourcePluginId, UUID request) {
			if (command.getCommand() == CommandsEnum.DISPLAY_COLOR) {
				// Call color change
			}
		}
	};
	private ControlConnectionManager.ControllConnectionManagerListenerAdapter clientListenr = new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {
		@Override
		public void stopControlling(String arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void controllRequest(CommandsEnum arg0, String arg1) {
			// TODO Auto-generated method stub
		}

		@Override
		public void consumeDisplayCommand(IDisplayCommand command, String sourcePluginId, UUID request) {
			if (command.getCommand() == CommandsEnum.DISPLAY_COLOR) {
				// Color fade
				Bundle config = new Bundle();
				// Setup previous RGB values
				config.putString("Action_Type", "fadeRGB2RGB");
				config.putString("R_Channel", Integer.toString(red));
				config.putString("G_Channel", Integer.toString(green));
				config.putString("B_Channel", Integer.toString(blue));
				// Store incoming RGB
				red = command.getR();
				green = command.getG();
				blue = command.getB();
				// Setup target RGB values
				config.putString("R_Channel2", Integer.toString(red));
				config.putString("G_Channel2", Integer.toString(green));
				config.putString("B_Channel2", Integer.toString(blue));
				config.putString("Fade_Period", "100");
				config.putString("Keep_On", "true");
				config.putString("Duration", "5000"); 
				handleRequest(null, config);
			}
		}
	};

	@Override
	public void start() {
		/*
		 * Nothing to do, since this is a pull plug-in... we're now waiting for context scan requests.
		 */
		Log.i(TAG, "Started");
		if (myArtnetManager == null) {
			// Artnet start discovery
			myArtnetManager = new ArtnetManager();
		}
		// test.startDiscovery();
		ArtnetManager.startListening(this);
		myArtnetManager.startDiscovery();
	}

	@Override
	public void stop() {
		/*
		 * At this point, the plug-in should cancel any ongoing context scans, if there are any.
		 */
		Log.d(TAG, "Stopped!");
		if (myArtnetManager != null) {
			if (discoveredNodeList != null && discoveredNodeList.size() > 0) {
				myArtnetManager.switchLightOff(discoveredNodeList.get(0));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
			myArtnetManager.stopDiscovery();
		}
	}

	@Override
	public void destroy() {
		/*
		 * At this point, the plug-in should release any resources.
		 */
		stop();
		myArtnetManager = null;
		Log.i(TAG, "Destroyed!");
	}

	public void setPowerScheme(PowerScheme scheme) {
		// Not supported
	}

	@Override
	public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
		Log.i(TAG, "init is called");
		this.setPowerScheme(scheme);
		if (loadSettings(settings)) {
			getPluginFacade().setPluginConfiguredStatus(getSessionId(), true);
		}
		controlMgr = new ControlConnectionManager(this, clientListenr, serverListener, new TranslatingProfileMatcher(),
				TAG);
	}

	@Override
	public void updateSettings(ContextPluginSettings settings) {
		/*
		 * Simple example of how to store settings and set the plug-in configured using the IPluginFacade.
		 */
		if (loadSettings(settings)) {
			getPluginFacade().storeContextPluginSettings(getSessionId(), settings);
			getPluginFacade().setPluginConfiguredStatus(getSessionId(), true);
		}
	}

	@Override
	public boolean addContextlistener(ContextListenerInformation listenerInfo) {
		return controlMgr.addContextListener(listenerInfo);
	}

	@Override
	public void onMessageReceived(Message message) {
		try {
			controlMgr.handleConfigCommand(message);
		} catch (RemoteException e) {
			Log.w(TAG, e.toString());
		}
	}

	/*
	 * Simple example of how to parse plug-in settings using key-value pairs.
	 */
	private boolean loadSettings(ContextPluginSettings settings) {
		// Check settings type and store
		if (settings != null) {
			Log.i(TAG, "Received previously stored settings: " + settings);
			try {
				sampleData = settings.get(SAMPLE_DATA);
				return true;
			} catch (Exception e) {
				Log.w(TAG, "Failed to parse settings: " + e.getMessage());
			}
		} else if (settings == null) {
			// Create default settings
			Log.i(TAG, "No settings found!");
		}
		return false;
	}

	@Override
	public void handleConfiguredContextRequest(UUID requestId, String contextInfoType, Bundle config) {
		// TODO Auto-generated method stub
//		Bundle mBundle1 = new Bundle();
//		mBundle1.putString("Action_Type", "fadewhite");
//		mBundle1.putString("Fade_Period", "101");
//		mBundle1.putString("Keep_On", "true");
//		mBundle1.putString("Duration", "5000"); // millisecond
//		Bundle mBundle4 = new Bundle();
//		mBundle4.putString("Action_Type", "fadeRGB");
//		mBundle4.putString("R_Channel", "101");
//		mBundle4.putString("G_Channel", "101");
//		mBundle4.putString("B_Channel", "101");
//		mBundle4.putString("Fade_Period", "101");
//		mBundle4.putString("Keep_On", "true");
//		mBundle4.putString("Duration", "5000"); // millisecond 
//		Bundle mBundle5 = new Bundle();
//		mBundle5.putString("Action_Type", "fadeRGB2RGB");
//		mBundle5.putString("R_Channel", "101");
//		mBundle5.putString("G_Channel", "101");
//		mBundle5.putString("B_Channel", "101");
//		mBundle5.putString("R_Channel2", "101");
//		mBundle5.putString("G_Channel2", "101");
//		mBundle5.putString("B_Channel2", "101");
//		mBundle5.putString("Fade_Period", "101");
//		mBundle5.putString("Keep_On", "true");
//		mBundle5.putString("Duration", "5000"); // millisecond
//		Bundle mBundle2 = new Bundle();
//		mBundle2.putString("Action_Type", "setcolor");
//		mBundle2.putString("R_Channel", "101");
//		mBundle2.putString("G_Channel", "101");
//		mBundle2.putString("B_Channel", "101");
//		Bundle mBundle3 = new Bundle();
//		mBundle3.putString("Action_Type", "resetcolor");
		if (!controlMgr.handleConfiguredContextRequest(requestId, contextInfoType, config)) {
			handleRequest(requestId, config);
		}
	}

	private void handleRequest(UUID requestId, Bundle scanConfig) {
		String actionType = scanConfig.getString("action_type");
		int fadePeriod = 500; // default
		boolean keepOn = false; // default
		int duration = 1000; // default
		// white
		int channelR = 0;
		int channelG = 0;
		int channelB = 0;
		// white
		int channelR2 = 0;
		int channelG2 = 0;
		int channelB2 = 0;
		if (actionType == null) {
			Log.e(TAG, "No action type found");
			sendContextRequestError(requestId, "No Action Type Found", ErrorCodes.CONFIGURATION_ERROR);
			return;
		}
		if (discoveredNodeList.size() <= 0) {
			Log.e(TAG, "No action to be taken, No nodes exists");
			sendContextRequestError(requestId, "No nodes found", ErrorCodes.NOT_FOUND);
			return;
		}
		if (actionType.compareTo("fadewhite") == 0) {
			Log.i(TAG, "Fade white");
			String temp = scanConfig.getString("fade_period");
			if (temp != null)
				fadePeriod = Integer.parseInt(temp);
			temp = scanConfig.getString("keep_on");
			if (temp != null)
				if (temp.compareTo("true") == 0)
					keepOn = true;
			temp = scanConfig.getString("duration");
			if (temp != null)
				duration = Integer.parseInt(temp);
			myArtnetManager.colorFade(discoveredNodeList.get(0), -1, fadePeriod, keepOn, duration); // -1 for white
																									// channel
			Log.i(TAG, "Fade white config: Fade Period[" + fadePeriod + "] KeepOn[" + keepOn + "] Duration[" + duration
					+ "]");
			if (requestId != null)
				sendContextRequestSuccess(requestId);
		} else if (actionType.compareTo("fadergb") == 0) {
			Log.i(TAG, "Fade RGB");
			String temp = scanConfig.getString("fade_period");
			if (temp != null)
				fadePeriod = Integer.parseInt(temp);
			temp = scanConfig.getString("keep_on");
			if (temp != null)
				if (temp.compareTo("true") == 0)
					keepOn = true;
			temp = scanConfig.getString("duration");
			if (temp != null)
				duration = Integer.parseInt(temp);
			temp = scanConfig.getString("r_channel");
			if (temp != null)
				channelR = Integer.parseInt(temp);
			temp = scanConfig.getString("g_channel");
			if (temp != null)
				channelG = Integer.parseInt(temp);
			temp = scanConfig.getString("b_channel");
			if (temp != null)
				channelB = Integer.parseInt(temp);
			myArtnetManager.fadeRGB(discoveredNodeList.get(0), channelR, channelG, channelB, fadePeriod, keepOn,
					duration);
			Log.i(TAG, "Fade RGB config: Fade Period[" + fadePeriod + "] RGB color[" + channelR + "," + channelG + ","
					+ channelB + "] KeepOn[" + keepOn + "] Duration[" + duration + "]");
			if (requestId != null)
				sendContextRequestSuccess(requestId);
		} else if (actionType.compareTo("fadergb2rgb") == 0) {
			Log.i(TAG, "Fade RGB Color 1 to RRGB Color 2");
			String temp = scanConfig.getString("fade_period");
			if (temp != null)
				fadePeriod = Integer.parseInt(temp);
			temp = scanConfig.getString("keep_on");
			if (temp != null)
				if (temp.compareTo("true") == 0)
					keepOn = true;
			temp = scanConfig.getString("duration");
			if (temp != null)
				duration = Integer.parseInt(temp);
			temp = scanConfig.getString("r_channel");
			if (temp != null)
				channelR = Integer.parseInt(temp);
			temp = scanConfig.getString("g_channel");
			if (temp != null)
				channelG = Integer.parseInt(temp);
			temp = scanConfig.getString("b_channel");
			if (temp != null)
				channelB = Integer.parseInt(temp);
			temp = scanConfig.getString("r_channel2");
			if (temp != null)
				channelR2 = Integer.parseInt(temp);
			temp = scanConfig.getString("g_channel2");
			if (temp != null)
				channelG2 = Integer.parseInt(temp);
			temp = scanConfig.getString("b_channel2");
			if (temp != null)
				channelB2 = Integer.parseInt(temp);
			myArtnetManager.fade2RGBColors(discoveredNodeList.get(0), channelR, channelG, channelB, channelR2,
					channelG2, channelB2, fadePeriod, keepOn, duration);
			Log.i(TAG, "Fade RGB to RGB config: Fade Period[" + fadePeriod + "RGB color[" + channelR + "," + channelG
					+ "," + channelB + " to RGB color[" + channelR2 + "," + channelG2 + "," + channelB2 + "] KeepOn["
					+ keepOn + "] Duration[" + duration + "]");
			if (requestId != null)
				sendContextRequestSuccess(requestId);
		} else if (actionType.compareTo("setcolor") == 0) {
			Log.e(TAG, "Set color");
			String temp;
			temp = scanConfig.getString("r_channel");
			if (temp != null)
				channelR = Integer.parseInt(temp);
			temp = scanConfig.getString("g_channel");
			if (temp != null)
				channelG = Integer.parseInt(temp);
			temp = scanConfig.getString("b_channel");
			if (temp != null)
				channelB = Integer.parseInt(temp);
			myArtnetManager.setNodeRGB(discoveredNodeList.get(0), channelR, channelG, channelB);
			Log.i(TAG, "Set RGB config: RGB color[" + channelR + "," + channelG + "," + channelB + "]");
			if (requestId != null)
				sendContextRequestSuccess(requestId);
		} else if (actionType.compareTo("resetcolor") == 0) {
			myArtnetManager.restRGB(discoveredNodeList.get(0));
			Log.i(TAG, "Reset color");
			if (requestId != null)
				sendContextRequestSuccess(requestId);
		} else {
			Log.e(TAG, "Action type is not supported");
			if (requestId != null)
				sendContextRequestError(requestId, "Action Type Not Supported " + actionType,
						ErrorCodes.CONFIGURATION_ERROR);
		}
	}

	// ============== Artnet methods
	@Override
	public void onStartedListening() {
		// TODO Auto-generated method stub
		Log.i(TAG, "Started Listening to the Artnet Manager");
	}

	@Override
	public void onStartedDiscovery() {
		// TODO Auto-generated method stub
		Log.i(TAG, "Artnet Manager started the discovery process");
	}

	@Override
	public void onStoppedDiscovery() {
		// TODO Auto-generated method stub
		Log.i(TAG, "Artnet Manager stopped the discovery process");
	}

	@Override
	public void onDiscoveryIsEnabled() {
		// TODO Auto-generated method stub
		Log.i(TAG, "Artnet Manager already discoverying");
	}

	@Override
	public void onDiscoveredNewNode(ArtNetNode node) {
		Log.i(TAG, "New node is detected " + node);
		Bundle config = new Bundle();
		config.putString("Action_Type", "fadewhite");
		config.putString("Fade_Period", "500");
		config.putString("Keep_On", "true");
		config.putString("Duration", "5000"); // millisecond
		handleRequest(null, config);
	}

	@Override
	public void onDiscoveredNodeDisconnected(ArtNetNode node) {
		// TODO Auto-generated method stub
		Log.i(TAG, "Node is disconnected " + node);
	}

	@Override
	public void onDiscoveryCompleted(List<ArtNetNode> nodes) {
		// TODO Auto-generated method stub
		Log.i(TAG, "Discovery is completed");
	}

	@Override
	public void onDiscoveryCompleteUpadate(List<ArtNetNode> nodes) {
		// TODO Auto-generated method stub
		Log.i(TAG, "Discovery returns new node items");
		discoveredNodeList = nodes;
		// myArtnetManager.colorFade(discoveredNodeList.get(0), -1, 3000, true, 1000);
	}

	@Override
	public void onDiscoveryFailed(Throwable t) {
		// TODO Auto-generated method stub
		Log.i(TAG, "Discovery failed");
	}
}